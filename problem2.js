//Find all users staying in Germany.
function findStayingInGermany(users){
    const germanyList=[];
    for(let index in users){
       if(users[index].nationality=="Germany"){
        germanyList.push(index);
       } 
    }
    return germanyList;
}
module.exports=findStayingInGermany;