// Find all users with masters Degree.
function findMasterDegreeUser(users){
    const masterDegreeList=[];
    for(let index in users){
        if(users[index].qualification=="Masters"){
            masterDegreeList.push(index)
        }
    }
    return masterDegreeList;
}
module.exports=findMasterDegreeUser;