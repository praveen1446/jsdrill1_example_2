function groupUsersBasedOnTheirProgrammingLanguage(users){
    let groupUsers=[];
    const language=["Javascript","Golang","Python"];
    for(let i=0;i<language.length;i++){
        groupUsers[language[i]]=[];
    }
    for(let name in users){
        const design=users[name]["desgination"];
        for(let i=0;i<language.length;i++){
            if(design.includes(language[i])){
                groupUsers[language[i]].push(name);
            }
        }
    }
    return groupUsers;
}
module.exports=groupUsersBasedOnTheirProgrammingLanguage;
